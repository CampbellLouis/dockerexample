FROM node:latest
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build:ssr
EXPOSE 4200
ENV test = "hello"
CMD [ "node", "dist/server" ]